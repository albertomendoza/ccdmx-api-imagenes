/**
 * ImagenesController
 *
 * @description :: Server-side logic for managing imagenes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	all: function (req, res) {
        Imagenes.find()
            .exec(function (error, imagenes){
                if (error) return res.send(400, {"status":"error"});
                if (req.isSocket) {
				    Imagenes.watch(req);
				    Imagenes.subscribe(req.socket, imagenes);
			    }
                return res.status(200).json(imagenes);
            });
    },
    create: function (req, res) {
		var dataImage = req.body;
        Imagenes.create(dataImage, function (error, imagen) {
          if (error) return res.send(400, {"status":"error"});
          Imagenes.publishCreate(imagen);
          return res.status(201).json(imagen);
        });
	},
};

